package com.example.dialogbugexample

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.dialogbugexample.ui.theme.DialogBugExampleTheme
import com.facebook.flipper.android.AndroidFlipperClient
import com.facebook.flipper.core.FlipperClient
import com.facebook.flipper.plugins.inspector.DescriptorMapping
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin
import com.facebook.soloader.SoLoader
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        SoLoader.init(application, false)
        val client: FlipperClient = AndroidFlipperClient.getInstance(application)
        client.addPlugin(InspectorFlipperPlugin(application, DescriptorMapping.withDefaults()))
        client.start()

        setContent {
            DialogBugExampleTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ComposeAlertDialog {
                        showDialog(this)
                    }
                }
            }
        }
    }

    private fun showDialog(context: Context) {
        MaterialAlertDialogBuilder(context)
            .setTitle("MaterialAlertDialogBuilder")
            .setPositiveButton("Yes") { _, _ ->
            }
            .show()
    }
}

@Composable
fun ComposeAlertDialog(onMaterialClick: () -> Unit) {
    val showDialog = remember { mutableStateOf(false) }
    if (showDialog.value) {
        AlertDialog(
            title = {
                Text("ComposeAlert")
            },
            onDismissRequest = {},
            confirmButton = {
                TextButton(onClick = {
                    showDialog.value = false
                }) {
                    Text("OK")
                }
            },
        )
    }
    Column(
        modifier = Modifier.padding(20.dp)
    ) {
        Button(
            onClick = {
                showDialog.value = true
            },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(text = "ComposeAlert")
        }
        Button(
            onClick = {
                onMaterialClick.invoke()
            },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(text = "MaterialAlertDialogBuilder")
        }
    }
}
